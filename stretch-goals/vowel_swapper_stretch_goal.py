def vowel_swapper(string):
    # ==============
    # Your code here
    counts = {'a': 0, 'e': 0, 'i': 0, 'o': 0, 'O': 0, 'u': 0}
    character_list = list(string)
    for index, ch in enumerate(character_list):
        if ch.lower() == 'a':
            counts['a'] += 1
            if counts['a'] == 2:
                character_list[index] = '/\\'
        elif ch.lower() == 'u':
            counts['u'] += 1
            if counts['u'] == 2:
                character_list[index] = '\/'
        elif ch.lower() == 'e':
            counts['e'] += 1
            if counts['e'] == 2:
                character_list[index] = '3'
        elif ch.lower() == 'i':
            counts['i'] += 1
            if counts['i'] == 2:
                character_list[index] = '!'
        elif ch == 'o' or ch == 'O':
            counts['o'] += 1
            if counts['o'] == 2 and ch == 'o':
                character_list[index] = 'ooo'
            elif counts['o'] == 2 and ch == 'O':
                character_list[index] = '000'

    swapped_vowels = ''.join(character_list)
    return swapped_vowels
    # ==============

print(vowel_swapper("aAa eEe iIi oOo uUu")) # Should print "a/\a e3e i!i o000o u\/u" to the console
print(vowel_swapper("Hello World")) # Should print "Hello Wooorld" to the console 
print(vowel_swapper("Everything's Available")) # Should print "Ev3rything's Av/\!lable" to the console
