def factors(number):
    # ==============
    # Your code here
    factors_list = [i for i in range(2, number) if number % i == 0]
    if len(factors_list) < 1:
        return f'{number} is a prime number.'
    else:
        return factors_list
    # ==============

print(factors(15)) # Should print [3, 5] to the console
print(factors(12)) # Should print [2, 3, 4, 6] to the console
print(factors(13)) # Should print “13 is a prime number”
