def vowel_swapper(string):
    # ==============
    # Your code here
    swapped_vowels = string.replace("o", "ooo").replace("O", "000").lower().replace("a", "4").lower().replace("e", "3").lower().replace("i", "!").lower().replace("u", "|_|")
    return swapped_vowels
    # ==============

print(vowel_swapper("aA eE iI oO uU")) # Should print "44 33 !! ooo000 |_||_|" to the console
print(vowel_swapper("Hello World")) # Should print "H3llooo Wooorld" to the console 
print(vowel_swapper("Everything's Available")) # Should print "3v3ryth!ng's 4v4!l4bl3" to the console